/**
 *****************************************************************************
 * @title   SPI.h
 * @author  Daniel Schnell <dschnell@posteo.de>
 * @brief   Timer Interface
 *******************************************************************************/

#ifndef __XTIMERS_H__
#define __XTIMERS_H__

#ifdef __cplusplus
extern "C"
{
#endif

// includes
#include "stm32f0xx.h"
#include "stm32f0xx_tim.h"

// defines
#define COLOR_RGB_TYPE_WS2811_N_BITS    24

// typedefs
typedef uint8_t     COLOR_RGB_TYPE_T[3];                                        // 8bits per color r,g,b
typedef uint16_t    COLOR_RGB_TYPE_WS2811_TIM_T[COLOR_RGB_TYPE_WS2811_N_BITS];


// function definitions

bool xtimers_init(uint32_t frequency);
bool xtimers_start(const COLOR_RGB_TYPE_T* rgb_buf, size_t rgb_n_elems);

#ifdef __cplusplus
}
#endif

#endif /* __XTIMERS_H__ */

/* EOF */
